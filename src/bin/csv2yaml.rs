use anyhow::Result;
use cobaltthief::data::*;
use serde::Deserialize;
use std::env::args_os;
use std::fs::{read_to_string, File};

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Row {
    pub track: String,
    pub duration: String,
    pub references: String,
    pub artists: String,
    pub contributors: String,

    #[serde(rename = "Cover Artists")]
    pub cover_artists: String,

    #[serde(rename = "URLs")]
    pub urls: String,

    #[serde(rename = "Art Tags")]
    pub art_tags: String,
}

#[derive(Debug)]
struct CsvTrack {
    pub track: String,
    pub duration: String,
    pub references: Vec<String>,
    pub artists: Vec<String>,
    pub contributors: Vec<String>,
    pub cover_artists: Vec<String>,
    pub urls: Vec<String>,
    pub art_tags: Vec<String>,
}

fn split_list(list: &str) -> Vec<String> {
    list.split(&['\r', '\n'])
        .map(|x| x.trim_start_matches("- ").to_string())
        .filter(|x| !x.is_empty())
        .collect()
}

fn clean_duration(duration: &str) -> String {
    let mut out = duration
        .trim_start_matches('0')
        .split(':')
        .take(2)
        .collect::<Vec<&str>>()
        .join(":");

    if out.starts_with(':') {
        out = "0".to_string() + &out;
    }

    out
}

impl From<Row> for CsvTrack {
    fn from(row: Row) -> Self {
        Self {
            track: row.track,
            duration: clean_duration(&row.duration),
            references: split_list(&row.references),
            artists: split_list(&row.artists),
            contributors: split_list(&row.contributors),
            cover_artists: split_list(&row.cover_artists),
            urls: split_list(&row.urls),
            art_tags: split_list(&row.art_tags),
        }
    }
}

fn main() -> Result<()> {
    let in_path = args_os().nth(1).unwrap();
    let out_path = args_os().nth(2).unwrap();

    let in_data = read_to_string(in_path)?;

    let mut reader = if in_data.starts_with("Track,Duration,") {
        csv::Reader::from_reader(in_data.as_bytes())
    } else {
        csv::ReaderBuilder::new()
            .has_headers(false)
            .delimiter(b'\t')
            .from_reader(in_data.as_bytes())
    };

    let mut out_file = File::create(out_path)?;

    for result in reader.deserialize() {
        let row: Row = result?;
        let csv_track: CsvTrack = row.into();
        let track = Track {
            track: csv_track.track,
            duration: Some(csv_track.duration).filter(|x| !x.is_empty()),
            referenced_tracks: csv_track.references,
            artists: csv_track.artists,
            contributors: csv_track.contributors,
            cover_artists: csv_track.cover_artists,
            urls: csv_track.urls,
            art_tags: csv_track.art_tags,

            has_urls: true,
            has_cover_art: true,

            ..Default::default()
        };

        serde_yaml::to_writer(&mut out_file, &track)?;
    }

    Ok(())
}
