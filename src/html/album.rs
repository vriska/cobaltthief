use super::Page;
use crate::data::*;
use maud::html;

impl Album {
    pub fn to_page(&self) -> Page {
        Page {
            title: self.album.clone(),
            stylesheet: "".to_string(),
            theme: "".to_string(),
            main: html! {
                h1 { (self.album) }
                p {
                    // TODO: artist names
                    @if !self.artists.is_empty() {
                        "By " /* ... */ "." br;
                    }

                    @if !self.cover_artists.is_empty() {
                        "Cover art by " /* ... */ "." br;
                    }

                    @if !self.wallpaper_artists.is_empty() {
                        "Wallpaper art by " /* ... */ "." br;
                    }

                    @if !self.banner_artists.is_empty() {
                        "Banner art by " /* ... */ "." br;
                    }

                    // TODO: better date formatting
                    @if let Some(date) = self.date {
                        "Released " (date) "." br;
                    }

                    @if let Some(date) = self.default_track_cover_art_date.filter(|x| self.date.map(|y| x != &y).unwrap_or(false)) {
                        "Art released " (date) "." br;
                    }

                    "Duration: " /* TODO: album duration */ "." br;
                }

                // TODO: additional files
                // TODO: commentary

                @if !self.urls.is_empty() {
                    p {
                        @for url in &self.urls {
                            "Listen on " (url) "." // TODO: format links
                        }
                    }
                }

                // TODO: track groups

                @let track_items = html! {
                    @for entry in &self.entries {
                        @if let AlbumEntry::Track(track) = entry {
                            li { (track.track) }
                        }
                    }
                };

                @if self.has_track_numbers {
                    ol { (track_items) }
                } @else {
                    ul { (track_items) }
                }

                @if let Some(date) = self.date_added {
                    p {
                        "Added to wiki " (date) "."
                    }
                }

                @if let Some(commentary) = &self.commentary {
                    p { "Artist commentary:" }
                    blockquote {
                        @for line in commentary.split('\n') {
                            p { (line) }
                        }
                    }
                }
            },
        }
    }
}
