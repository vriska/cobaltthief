use maud::{html, Markup, DOCTYPE};

mod album;

#[derive(Debug)]
pub struct Page {
    pub title: String,
    pub stylesheet: String,
    pub theme: String,
    pub main: Markup,
    // TODO: meta
    // TODO: banner
    // TODO: sidebars
    // TODO: nav
    // TODO: footer
    // TODO: embeds
}

impl Page {
    pub fn to_markup(&self) -> Markup {
        html! {
            (DOCTYPE)
            html {
                head {
                    title { (self.title) }
                    meta charset="utf-8";
                    meta name="viewport" content="width=device-width, initial-scale=1";

                    // TODO: canonical

                    link rel="stylesheet" href="/site.css";

                    @if !self.stylesheet.is_empty() || !self.theme.is_empty() {
                        style {
                            (self.stylesheet)
                            (self.theme)
                        }
                    }

                    script src="/lazy-loading.js" {}
                }

                body {
                    div #"page-container" {
                        // TODO: skippers

                        (self.main)
                    }

                    // TODO: infocard

                    script type="module" src="/client.js" {}
                }
            }
        }
    }
}
