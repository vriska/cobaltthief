use anyhow::Result;
use cobaltthief::data::*;
use std::env::args_os;

fn main() -> Result<()> {
    let album = Album::from_path(args_os().nth(1).unwrap())?;

    println!("{}", album.to_page().to_markup().into_string());

    Ok(())
}
