use anyhow::Result;
use csscolorparser::Color;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::fs::{read_to_string, File};
use std::path::Path;
use time::Date;

pub mod date_format {
    use serde::{de, Deserializer, Serialize, Serializer};
    use std::fmt;
    use time::{
        format_description::{modifier::*, *},
        Date,
    };

    const DESCRIPTION: &[FormatItem<'_>] = &[
        FormatItem::Component(Component::Month({
            let mut value = Month::default();
            value.repr = MonthRepr::Long;
            value
        })),
        FormatItem::Literal(b" "),
        FormatItem::Component(Component::Day({
            let mut value = Day::default();
            value.padding = Padding::None;
            value
        })),
        FormatItem::Optional(&FormatItem::Literal(b",")),
        FormatItem::Literal(b" "),
        FormatItem::Component(Component::Year(Year::default())),
        FormatItem::Optional(&FormatItem::Compound(&[
            FormatItem::Literal(b" "),
            FormatItem::Component(Component::Hour(Hour::default())),
            FormatItem::Literal(b":"),
            FormatItem::Component(Component::Minute(Minute::default())),
            FormatItem::Literal(b":"),
            FormatItem::Component(Component::Second(Second::default())),
        ])),
    ];

    struct Visitor;

    impl<'a> de::Visitor<'a> for Visitor {
        type Value = Date;

        fn expecting(&self, f: &mut fmt::Formatter<'_>) -> ::std::fmt::Result {
            write!(
                f,
                "a(n) `Date` in the format \"[month repr:long] [day padding:none], [year]\""
            )
        }

        fn visit_str<E: de::Error>(self, value: &str) -> Result<Date, E> {
            Date::parse(value, &DESCRIPTION).map_err(E::custom)
        }
    }

    struct OptionVisitor;

    impl<'a> ::serde::de::Visitor<'a> for OptionVisitor {
        type Value = Option<Date>;

        fn expecting(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
            write!(
                f,
                "an `Option<Date>` in the format \"[month repr:long] [day padding:none], [year]\""
            )
        }

        fn visit_some<D: Deserializer<'a>>(
            self,
            deserializer: D,
        ) -> Result<Option<Date>, D::Error> {
            deserializer.deserialize_any(Visitor).map(Some)
        }

        fn visit_none<E: de::Error>(self) -> Result<Option<Date>, E> {
            Ok(None)
        }
    }

    pub fn serialize<S: Serializer>(datetime: &Date, serializer: S) -> Result<S::Ok, S::Error> {
        datetime
            .format(&DESCRIPTION)
            .map_err(time::error::Format::into_invalid_serde_value::<S>)?
            .serialize(serializer)
    }

    pub fn deserialize<'a, D: Deserializer<'a>>(deserializer: D) -> Result<Date, D::Error> {
        deserializer.deserialize_any(Visitor)
    }

    pub(super) mod option {
        use super::{OptionVisitor, DESCRIPTION};
        use serde::{Deserializer, Serialize, Serializer};
        use time::Date;

        pub fn serialize<S: Serializer>(
            option: &Option<Date>,
            serializer: S,
        ) -> Result<S::Ok, S::Error> {
            option
                .map(|datetime| datetime.format(&DESCRIPTION))
                .transpose()
                .map_err(time::error::Format::into_invalid_serde_value::<S>)?
                .serialize(serializer)
        }

        pub fn deserialize<'a, D: Deserializer<'a>>(
            deserializer: D,
        ) -> Result<Option<Date>, D::Error> {
            deserializer.deserialize_option(OptionVisitor)
        }
    }
}

fn parse_from_path<T: DeserializeOwned>(path: impl AsRef<Path>) -> Result<Vec<T>> {
    let file = read_to_string(path)?;
    file.split("\n---\n")
        .map(|x| Ok(serde_yaml::from_str(x)?))
        .collect()
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Artist {
    pub artist: String,
    pub directory: Option<String>,

    #[serde(default)]
    pub aliases: Vec<String>,

    #[serde(rename = "URLs", default)]
    pub urls: Vec<String>,

    #[serde(rename = "Dead URLs", default)]
    pub dead_urls: Vec<String>,

    #[serde(rename = "Context Notes")]
    pub context_notes: Option<String>,
}

impl Artist {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Vec<Self>> {
        parse_from_path(path)
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Act {
    pub color: Color,
    pub act: String,
    pub anchor: String,
    pub jump: Option<String>,

    #[serde(rename = "Jump Color")]
    pub jump_color: Option<Color>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Flash {
    pub directory: Option<String>,

    #[serde(default)]
    pub contributors: Vec<String>,

    #[serde(with = "date_format")]
    pub date: Date,

    #[serde(rename = "Cover Art File Extension")]
    pub cover_art_file_extension: Option<String>,

    #[serde(rename = "URLs", default)]
    pub urls: Vec<String>,

    #[serde(rename = "Featured Tracks", default)]
    pub featured_tracks: Vec<String>,

    pub flash: String,

    pub page: Option<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum FlashEntry {
    Act(Act),
    Flash(Flash),
}

impl FlashEntry {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Vec<Self>> {
        parse_from_path(path)
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Category {
    pub color: Color,
    pub category: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Group {
    pub directory: Option<String>,

    #[serde(rename = "URLs", default)]
    pub urls: Vec<String>,

    pub description: String,
    pub group: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum GroupEntry {
    Category(Category),
    Group(Group),
}

impl GroupEntry {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Vec<Self>> {
        parse_from_path(path)
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Homepage {
    pub homepage: String,

    #[serde(rename = "Sidebar Content")]
    pub sidebar_content: String,

    #[serde(skip)]
    pub rows: Vec<HomepageRow>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum HomepageRowType {
    Albums,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct HomepageRow {
    pub row: String,
    pub r#type: HomepageRowType,
    pub group: String,
    pub count: usize,

    #[serde(default)]
    pub actions: Vec<String>,
}

impl Homepage {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Self> {
        let file = read_to_string(path)?;
        let mut sections = file.split("\n---\n");
        let first = sections.next().unwrap_or("");
        let mut this: Self = serde_yaml::from_str(first)?;

        this.rows = sections
            .map(|x| Ok(serde_yaml::from_str(x)?))
            .collect::<Result<_>>()?;

        Ok(this)
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct News {
    pub name: String,
    pub directory: String,

    #[serde(with = "date_format")]
    pub date: Date,

    pub content: String,
}

impl News {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Vec<Self>> {
        parse_from_path(path)
    }
}

fn default_true() -> bool {
    true
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct StaticPage {
    pub name: String,
    pub directory: String,

    #[serde(rename = "Short Name")]
    pub short_name: Option<String>,

    pub style: Option<String>,
    pub content: String,

    #[serde(
        rename = "Show in Navigation Bar",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub show_in_navigation_bar: bool,
}

impl StaticPage {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Vec<Self>> {
        parse_from_path(path)
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Tag {
    pub color: Option<Color>,
    pub tag: String,

    #[serde(rename = "Is CW", default, skip_serializing_if = "std::ops::Not::not")]
    pub is_cw: bool,
}

impl Tag {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Vec<Self>> {
        parse_from_path(path)
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct WikiInfo {
    pub name: String,
    pub color: Color,
    pub description: String,

    #[serde(rename = "Canonical Base")]
    pub canonical_base: String,

    #[serde(rename = "Short Name")]
    pub short_name: String,

    #[serde(rename = "Enable Flashes & Games")]
    pub enable_flashes_and_games: bool,

    #[serde(rename = "Enable Listings")]
    pub enable_listings: bool,

    #[serde(rename = "Enable News")]
    pub enable_news: bool,

    #[serde(rename = "Enable Art Tag UI")]
    pub enable_art_tag_ui: bool,

    #[serde(rename = "Enable Group UI")]
    pub enable_group_ui: bool,

    #[serde(rename = "Footer Content")]
    pub footer_content: String,
}

impl WikiInfo {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Self> {
        let file = File::open(path)?;
        let data = serde_yaml::from_reader(file)?;
        Ok(data)
    }
}

#[derive(Debug, Default, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Album {
    pub album: String,
    pub directory: Option<String>,

    #[serde(default)]
    pub artists: Vec<String>,

    #[serde(with = "date_format::option", default)]
    pub date: Option<Date>,

    #[serde(rename = "Date Added", with = "date_format::option", default)]
    pub date_added: Option<Date>,

    #[serde(rename = "URLs", default)]
    pub urls: Vec<String>,

    #[serde(
        rename = "Has Track Art",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub has_track_art: bool,

    #[serde(
        rename = "Has Cover Art",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub has_cover_art: bool,

    #[serde(
        rename = "Has Track Numbers",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub has_track_numbers: bool,

    #[serde(rename = "Cover Artists", default)]
    pub cover_artists: Vec<String>,

    #[serde(rename = "Cover Art File Extension")]
    pub cover_art_file_extension: Option<String>,

    #[serde(rename = "Track Art File Extension")]
    pub track_art_file_extension: Option<String>,

    #[serde(rename = "Default Track Cover Artists", default)]
    pub default_track_cover_artists: Vec<String>,

    pub color: Color,
    pub groups: Vec<String>,

    #[serde(rename = "Banner Artists", default)]
    pub banner_artists: Vec<String>,

    #[serde(rename = "Banner File Extension")]
    pub banner_file_extension: Option<String>,

    #[serde(rename = "Banner Dimensions")]
    pub banner_dimensions: Option<String>,

    #[serde(rename = "Banner Style")]
    pub banner_style: Option<String>,

    #[serde(rename = "Wallpaper Artists", default)]
    pub wallpaper_artists: Vec<String>,

    #[serde(rename = "Wallpaper Style")]
    pub wallpaper_style: Option<String>,

    #[serde(rename = "Wallpaper File Extension")]
    pub wallpaper_file_extension: Option<String>,

    #[serde(
        rename = "Default Track Cover Art Date",
        with = "date_format::option",
        default
    )]
    pub default_track_cover_art_date: Option<Date>,

    #[serde(rename = "Art Tags", default)]
    pub art_tags: Vec<String>,

    #[serde(rename = "Additional Files", default)]
    pub additional_files: Vec<AdditionalFile>,

    pub commentary: Option<String>,

    #[serde(
        rename = "Major Release",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub major_release: bool,

    #[serde(
        rename = "Listed on Homepage",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub listed_on_homepage: bool,

    #[serde(skip)]
    pub entries: Vec<AlbumEntry>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct AdditionalFile {
    pub title: String,
    pub files: Vec<String>,
}

#[derive(Debug, Default, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase", deny_unknown_fields)]
pub struct Track {
    pub track: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub directory: Option<String>,

    #[serde(
        rename = "Date First Released",
        with = "date_format::option",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub date_first_released: Option<Date>,

    #[serde(
        rename = "Originally Released As",
        skip_serializing_if = "Option::is_none"
    )]
    pub originally_released_as: Option<String>,

    #[serde(default)]
    pub artists: Vec<String>,

    #[serde(rename = "Art Tags", default, skip_serializing_if = "Vec::is_empty")]
    pub art_tags: Vec<String>,

    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub contributors: Vec<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub duration: Option<String>,

    #[serde(rename = "URLs", default, skip_serializing_if = "Vec::is_empty")]
    pub urls: Vec<String>,

    #[serde(
        rename = "Has URLs",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub has_urls: bool,

    #[serde(
        rename = "Has Cover Art",
        default = "default_true",
        skip_serializing_if = "bool::clone"
    )]
    pub has_cover_art: bool,

    #[serde(
        rename = "Cover Artists",
        default,
        skip_serializing_if = "Vec::is_empty"
    )]
    pub cover_artists: Vec<String>,

    #[serde(
        rename = "Cover Art Date",
        with = "date_format::option",
        default,
        skip_serializing_if = "Option::is_none"
    )]
    pub cover_art_date: Option<Date>,

    #[serde(
        rename = "Referenced Tracks",
        default,
        skip_serializing_if = "Vec::is_empty"
    )]
    pub referenced_tracks: Vec<String>,

    #[serde(
        rename = "Sampled Tracks",
        default,
        skip_serializing_if = "Vec::is_empty"
    )]
    pub sampled_tracks: Vec<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub lyrics: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub commentary: Option<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[allow(clippy::large_enum_variant)]
#[serde(untagged)]
pub enum AlbumEntry {
    Track(Track),
    Group {
        #[serde(rename = "Group")]
        group: String,
    },
}

impl Album {
    pub fn from_path(path: impl AsRef<Path>) -> Result<Self> {
        let file = read_to_string(path)?;
        let mut sections = file.split("\n---");
        let first = sections.next().unwrap_or("");
        let mut this: Self = serde_yaml::from_str(first)?;

        this.entries = sections
            .map(|x| Ok(serde_yaml::from_str(x)?))
            .collect::<Result<_>>()?;

        Ok(this)
    }
}
